# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Timesheets Bella',
    'version': '1.0',
    'category': 'Bella Project',
    'summary': 'Review and approve employees time reports',
    'description': """
        This module implements two filters in timesheet system.

        To filter administrative and operational tasks in timesheet reports.
    """,
    'author': "Jose Nicolielly",
    'website': "https://markdebrand.com/",
    'depends': ['hr', 'project'],
    'data': [
        #'data/hr_timesheet_data_bella.xml',
        'views/hr_timesheet_bella_views.xml',
    ],
    'demo': [

    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
