# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Bella Product Filter in Invoice and Quotation',
    'version' : '1.0',
    'summary': 'Bella Product Filter in Invoice and Quotation',
    'sequence': 30,
    'description': """
Bella Product Filter in Invoice and Quotation
    """,
    'category': 'Sales',
    'depends' : ['base',
                 'sale',
                 'account'],
    'data': [
        'views/invoice_quotation_filter.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}