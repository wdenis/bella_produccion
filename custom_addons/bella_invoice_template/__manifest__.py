# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Bella Invoice Template',
    'version' : '1.0',
    'summary': 'Bella template for invoices',
    'sequence': 30,
    'description': """
Bella template for invoices
    """,
    'category': 'Sales',
    'depends' : ['base',
                 'sale'],
    'data': [
        'views/account_report.xml',
        'views/account_view.xml',
        'views/res_company_view.xml',
        'security/ir.model.access.csv',
        'data/bella_invoice_template_init.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}