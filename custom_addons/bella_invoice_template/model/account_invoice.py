# -*- coding: utf-8 -*-

from odoo import api, fields, models
from . import amount_to_text
from odoo.exceptions import UserError
import time
from datetime import date, timedelta, datetime


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    invoice_note = fields.Char('Observaciones', size=200)
    dian_id = fields.Many2one('res.company.dian', 'Resolución Dian', compute='_compute_dian', store=True)

    @api.model
    def init_function_bella_invoice(self):
        obj_invoice = self.env['account.invoice'].search([('type','=','out_invoice'),('company_id','=',5)])
        for item in obj_invoice:
            item.dian_id = ''
            if item.date_invoice:
                invoice_date = item.date_invoice
            else:
                invoice_date = datetime.now().strftime('%Y-%m-%d')
            dian_obj = self.env['res.company.dian'].search([('res_company_id','=',item.company_id.id),
                                                            ('dian_resolution_date','<=',invoice_date)],
                                                           limit=1,
                                                           order='dian_resolution_date desc')
            if dian_obj:
                if dian_obj.dian_expiration_date >= invoice_date:
                    item.dian_id = dian_obj.id
                else:
                    item.dian_id = ''
                    #raise UserError('No existe ningún registro DIAN activo para la fecha %s' %(item.date_invoice))
            else:
                item.dian_id = ''
                #raise UserError('No existe ningún registro DIAN activo para la fecha %s' %(item.date_invoice)

    @api.depends('date_invoice','partner_id')
    def _compute_dian(self):
        for item in self:
            item.dian_id = ''
            if item.date_invoice:
                invoice_date = item.date_invoice
            else:
                invoice_date = datetime.now().strftime('%Y-%m-%d')
            dian_obj = self.env['res.company.dian'].search([('res_company_id','=',item.company_id.id),
                                                            ('dian_resolution_date','<=',invoice_date)],
                                                           limit=1,
                                                           order='dian_resolution_date desc')
            if dian_obj:
                if dian_obj.dian_expiration_date >= invoice_date:
                    item.dian_id = dian_obj.id
                else:
                    item.dian_id = ''
                    #raise UserError('No existe ningún registro DIAN activo para la fecha %s' %(item.date_invoice))
            else:
                item.dian_id = ''
                #raise UserError('No existe ningún registro DIAN activo para la fecha %s' %(item.date_invoice))

    @api.multi
    def _get_sales_order(self):
        for item in self:
            if item.origin:
                so_obj = self.env['sale.order'].search([('name','=',item.origin)])
                return so_obj
            else:
                return False

    @api.multi
    def _get_amount_text(self):
        for item in self:
            if item.amount_total:
                number = str(item.amount_total)
                text = amount_to_text.ToText(number)
                return text.escribir

    @api.multi
    def _get_format_date(self):
        for item in self:
            if item.dian_id and item.dian_id.dian_resolution_date:
                date = item.dian_id.dian_resolution_date.split('-')
                return date[2] + '/' + date[1] + '/' + date[0]

    @api.multi
    def _get_qty(self, qty):
        cant = int(qty)
        return cant

    @api.one
    def capitalize_text(self, text):
        return text.capitalize()

    @api.one
    def format_amount(self, amount):
        split_amount = str(amount).split('.')
        if len(split_amount) == 2:
            int_part = format(int(split_amount[0]), ',d')
            dec_part = split_amount[1]
            if len(dec_part) < 2:
                dec_part = dec_part + '0'
            new_amount = int_part + '.' + dec_part
            return new_amount
        else:
            return format(int(amount), ',d') + '.00'

    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.invoice_line_ids:
            if line.product_id and line.product_id.product_tmpl_id.categ_id.name == "Staffing":
                price_unit = ((line.price_unit * line.hours_per_day) * line.total_days) * (1 - (line.discount or 0.0) / 100.0)
            else:
                price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.invoice_line_tax_ids.compute_all(price_unit, self.currency_id, line.quantity, line.product_id,
                                                          self.partner_id)['taxes']
            for tax in taxes:
                val = self._prepare_tax_line_vals(line, tax)
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key(val)

                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
        return tax_grouped

    @api.one
    def margin(self, c=False):
        if c:
            sql = "UPDATE report_paperformat SET margin_left=15, margin_right=15 WHERE id=2"
        else:
            sql = "UPDATE report_paperformat SET margin_left=7, margin_right=7 WHERE id=2"
        self._cr.execute(sql)
        return True

    @api.onchange('partner_id')
    def get_terms_and_conditions(self):
        if self.partner_id:
            if self.env['ir.config_parameter'].sudo().get_param('sale.use_sale_note') and self.env.user.company_id.sale_note:
                self.comment = self.with_context(lang=self.partner_id.lang).env.user.company_id.sale_note
        else:
            self.comment = 'MARKDEBRAND'

class ResCompanyDian(models.Model):
    _name = 'res.company.dian'
    _rec_name = 'dian_resolution_number'

    res_company_id = fields.Many2one('res.company', 'Company id')
    dian_resolution_number = fields.Char('Resolución DIAN #')
    dian_resolution_date = fields.Date('Fecha de Resolución')
    dian_expiration_date = fields.Date('Fecha de Expiración', compute='_compute_expiration_date', store=True)
    dian_range_from = fields.Integer('Rango Desde')
    dian_range_to = fields.Integer('Rango Hasta')

    @api.depends('dian_resolution_date')
    def _compute_expiration_date(self):
        if self.dian_resolution_date:
            fecha = time.strptime(self.dian_resolution_date, '%Y-%m-%d')
            fecha = date(fecha.tm_year + 1, fecha.tm_mon, fecha.tm_mday) - timedelta(1)
            self.dian_expiration_date = fecha


    @api.onchange('dian_range_from','dian_range_to')
    def _on_change_range(self):
        if self.dian_range_from and self.dian_range_to:
            if self.dian_range_from > self.dian_range_to:
                raise UserError('El campo "Rango Desde" no puede ser superior al campo "Rango Hasta"')

class ResCompany(models.Model):
    _inherit = 'res.company'

    colombian_company = fields.Boolean(compute='_compute_colombian')
    dian = fields.One2many('res.company.dian','res_company_id', sring='Dian')

    @api.depends('country_id')
    def _compute_colombian(self):
        if self.country_id and self.country_id.name == 'Colombia':
            self.colombian_company = True
        else:
            self.colombian_company = False