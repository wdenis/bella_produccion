# -*- coding: utf-8 -*-
{
    'name': 'Timeseet x_billing_status',
    'category': 'Extra Tools',
    'summary': 'Add  billing status   on hr_timesheet',
    'version': '11.0.1.0.0',
    'author': 'MDB',
    'depends': [
        'base_setup',
         'hr_timesheet'
        ],
    'data': [
        'views/hr_timesheet.xml',
        ],
    'installable': True,
}
