# -*- coding: utf-8 -*-
from odoo import fields, models


class AccountAnalyticLine(models.Model):
       _inherit = 'account.analytic.line'
       x_billing_status  = fields.Selection([('billable','Billable'),('nonbillable','Non Billable')], 'Billing Status')