# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# 


{
    'name': 'Discount',
    'version': '1.1',
    'category': 'Sale',
    'author': 'William Denis',
    'maintainer': 'wdenis123.1@gmail.com',
    'description': """

    Module to manage discount per user in percentage. Add maximum discount functionality per user   

    """,
    'depends': ['base', 'sale_management'],
    'data': [
        'views/res_user_view.xml'
    ],
}
