# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class Users(models.Model):
    _inherit = 'res.users'

    maximum_discount = fields.Float('Maximum discount per sale', help="Maximum discount per sale as a percentage.")


