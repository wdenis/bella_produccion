# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class Partner(models.Model):
    _inherit = 'res.partner'

    targetbrand = fields.Boolean(string='Is a Target Brand', help="Check this box if this contact is a Target Brand.")
