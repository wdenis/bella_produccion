# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import uuid

from itertools import groupby
from datetime import datetime, timedelta
from werkzeug.urls import url_encode

from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError
from odoo.osv import expression
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

from odoo.tools.misc import formatLang

from odoo.addons import decimal_precision as dp
from odoo.exceptions import Warning


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"
    _description = "Sales Order Line"

    @api.model
    def create(self, values):
        line = super(SaleOrderLine, self).create(values)
        if line.discount:
            msg1 = self.env['res.users'].browse(line.create_uid.id)
            if line.discount > msg1.maximum_discount:
                raise Warning(_('This user does not have permission for this discount percentage. Please modify the discount percentage.'))
        return line

    @api.multi
    def write(self, values):
        values = self._discount_max(values)
        return super(SaleOrderLine, self).write(values)

    def _discount_max(self, values):
        if values.get('discount'):
            values = dict(values or {})
            msg1 = self.env['res.users'].browse(self._context['uid'])
            if values['discount'] > msg1.maximum_discount:
                    raise Warning(_('This user does not have permission for this discount percentage. Please modify the discount percentage.'))
        return values