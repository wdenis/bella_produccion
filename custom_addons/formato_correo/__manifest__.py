# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# 


{
    'name': 'formato_correo_cotizacion',
    'version': '1.1',
    'category': 'sale',
    'author': 'William Denis',
    'maintainer': 'wdenis123.1@gmail.com',
    'description': """

    Module to change the quote template and default these templates in order to adjust to colors and parameters  

    """,
    'depends': ['base', 'sale'],
    'data': [
        'data/mail_template_data.xml'
    ],
}
