# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'HR Additional Security',
    'version': '1.0',
    'category': 'hr',
    'description': """

    HR Additional Security

    """,
    'depends': ['base', 'hr'],
    'data': [
        'security/hr_security.xml',
        'views/hr_views.xml',
    ],
}
