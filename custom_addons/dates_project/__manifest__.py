# -*- coding: utf-8 -*-
{
    'name': "dates_project",

    'summary': """
        In the creation of projects place the following fields
        """,

    'description': """
        In the creation of projects place the following fields:
     - Project Start Date
     - Project end date
     - Reassignment of date
    """,

    'author': "Rafael E. Requena G.",
    'website': "https://bella.markdebrand.com",
    'category': 'Project',
    'version': '0.1',
    'depends': ['base','project'],
    'data': [
        'views/project_views.xml',
     
    ],    
    'demo': [
    
    ],
}