# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api
from odoo import fields, models,_


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    consecutive = fields.Char('Number',default=lambda x: _('New'))
    consecutive_active = fields.Boolean(default=False)

    @api.multi
    def action_move_create(self):
        result = super(AccountInvoice, self).action_move_create()
        for inv in self:
            context = dict(self.env.context)
            # Within the context of an invoice,
            # this default value is for the type of the invoice, not the type of the asset.
            # This has to be cleaned from the context before creating the asset,
            # otherwise it tries to create the asset with the type of the invoice.
            context.pop('default_type', None)
            if inv.type == "in_refund" or inv.type == "out_refund":
                inv.number = inv.consecutive
            inv.invoice_line_ids.with_context(context).asset_create()
        return result