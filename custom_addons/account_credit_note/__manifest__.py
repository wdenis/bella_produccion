# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "Credit Notes on Invoice",
    "version": "8.0.0.0.6",
    "author": "Bella",
    "category": "Generic Modules/Accounting",
    "license": "",
    "depends": [
        "base",
        "account"
    ],
    "demo": [],
    "data": [
        'data/ir_sequence_data.xml',
        'views/account_invoice_view.xml',
    ],
    "test": [],
    "js": [],
    "css": [],
    "qweb": [],
    "installable": True,
    "auto_install": False,
}
