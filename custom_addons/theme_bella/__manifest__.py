# {
#   'nombre': 'Tema de Bella',
#   'descripción': 'Tema principal para Bella',
#   'versión': '1.0',
#   'autor': 'Bella',
#
#   'datos':  [
#   ],
#   'categoría': 'Tema / Creativo',
#   'depende':  ['website'],
# }

{
    'name': 'Tema de Bella',
    'description': 'BElla website theme to showcase customization possibilities.',
    'category': 'Theme/Hidden',
    'version': '1.0',
    'depends': ['website'],
    'data': [
            'views/layout.xml',
            # 'views/pages.xml',
            'views/assets.xml',
            # 'views/company.xml',
    ],
    'images': [

    ],
    'application': False,
}
