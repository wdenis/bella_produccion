# -*- coding: utf-8 -*-

from odoo import fields, models


class ResPartner(models.Model):

    _inherit = 'res.partner'


    x_market = fields.Selection([('anglo','Anglo'),('latin','Latin'),('europe','Europe'),('asia','Asia')],'Market')
