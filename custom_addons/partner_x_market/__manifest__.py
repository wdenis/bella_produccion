# -*- coding: utf-8 -*-
{
    'name': 'Partner x_market',
    'category': 'Extra Tools',
    'summary': 'Add market on partner',
    'version': '11.0.1.0.0',
    'author': 'MDB',
    'depends': [
        'base_setup'
        ],
    'data': [
        'views/res_partner.xml',
        ],
    'installable': True,
}
