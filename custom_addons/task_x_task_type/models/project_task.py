# -*- coding: utf-8 -*-
from odoo import fields, models


class ProjectProject(models.Model):
    _inherit = "project.task"
    x_task_type  = fields.Selection([('adminisdtrative','Administrative'),('operative','Operative')],'Type of Task')
