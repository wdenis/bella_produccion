# -*- coding: utf-8 -*-
{
    'name': 'Project  x_task_type',
    'category': 'Extra Tools',
    'summary': 'Add  task  type on project',
    'version': '11.0.1.0.0',
    'author': 'MDB',
    'depends': [
        'base_setup',
         'project'
        ],
    'data': [
        'views/project_task.xml',
        ],
    'installable': True,
}
