# -*- coding: utf-8 -*-
{
     'name': "Custom css Web",
     'description': "Add your custom css in web",
     'category': 'CSS',
     'summary': 'Add your custom css in web',
    'version': '11.0.1.0.0',
    'author': 'MDB',
     'depends': ['web'],
     'css': [
	       'static/src/css/my_backend.css',
               'static/src/css/my_frontend.css'
     ],
    
     'data': ['view/template.xml']
 }