# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'CRM Bella',
    'version': '1.0',
    'category': 'crm',
    'description': """
    CRM Bella
    """,
    'depends': ['base', 'crm', 'sale_crm', 'mail','sale','account','product','project'],
    'data': [
        'security/ir.model.access.csv',
        'data/crm_bella_config_data.xml',
        'views/crm_lead_views.xml',
        'data/crm_bella_function_data.xml',
        'views/project.xml'
    ],
}
