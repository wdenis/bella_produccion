��    E      D  a   l      �     �     �                    +  <   ?     |  
   �  
   �     �     �  	   �     �     �     �  b   �     F     I     a     r     �     �     �     �  
   �  
   �  	   �     �     �     �     �  	   �  z     #   }  	   �     �     �     �     �     �     �     �     �     		     	  	   $	     .	  	   ?	     I	  L   V	     �	  @   �	  B   �	  ]   -
     �
  (   �
     �
  5   �
  �        �     �     �     �     �     �            C  +  	   o  
   y     �     �     �     �  <   �     �  
     	                  $     1     K     X  �   h     �     �          &     @     Z     q     }  
   �     �     �     �     �     �     �     �  z   �  +   l  	   �     �     �     �     �     �     �     �     �       	        "     .  
   H     S  L   Y     �  B   �  B   �  ]   3     �  (   �  -   �  1   �  �   '     �     �     �     �     �               /         4   	   .      
   8                 9            5          &   6   B       C          )          =   ,   +   ?              ;         0      A       $          2      '       :           3   /   %                       !   D   7   *       (                                   "                  #      E   -      <              @          >   1        Activity Assigned to Brief Client Configuration Sub_Tasks Configuration Tasks Create a project task conf, the first step of a new project. Created Created by Created on DNI Date Days task Describe the target brand... Description Display Name Estimated time to do the task, usually set by the project manager when the task is in draft state. ID Initially Planned Hours Last Modified on Last Updated by Last Updated on Lead/Opportunity Leads Lines sub-task Lines task Load Tasks Mark Lost Mark Won NEW Name Name Sub_task Name task Note that once a Brief becomes a Sale Order, it will be moved
                from the Brief list to the Sales Order list. Only a draft payment can be posted. Operative Opportunity Paid Payments Presentation Product Product Template Project Sub_Task Project Task Project Task Conf Prospect Quotation Sales Order Line Sub_Tasks Target Brand Task Created (%s): <a href=# data-oe-model=project.task data-oe-id=%d>%s</a> Tasks The payment cannot be processed because the invoice is not open! The product of the invoice is not configured in the project tasks. This task has been created from: <a href=# data-oe-model=sale.order data-oe-id=%d>%s</a> (%s) Type of Task You can't "Mark Done" this lead until %s You can't edit this lead stage. You have to define a sequence for %s in your company. Your next actions should flow efficiently: confirm the Brief
                to a Sale Order, then create the Invoice and collect the Payment. name product.sub.task product.sub.task.name product.task.conf project.sub.task project.sub.task.conf project.task.conf sub.task.name Project-Id-Version: Odoo Server 11.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-08-15 15:31+0000
PO-Revision-Date: 2018-08-15 11:45-0400
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es
X-Generator: Poedit 1.8.7.1
 Actividad Asignada a Brief Ganado Configuración Sub_Tareas Configuración Tareas Create a project task conf, the first step of a new project. Creada Creado por Creado el DNI Fecha Días Tareas Describe la iniciativa... Descripción Nombre mostrado Tiempo estimado para realizar la tarea, normalmente fijado por el responsable del proyecto cuando la tarea está en estado borrador. ID Horas iniciales planificadas Última Modificación en Última actualización de Última Actualización el Iniciativa/Oportunidad Iniciativas Lines sub-task Lines task Cargar Tareas Marcar perdido Marcar como ganado NEW Nombre Nombre Sub_Tarea Nombre  Tarea Note that once a Brief becomes a Sale Order, it will be moved
                from the Brief list to the Sales Order list. Solo se puede publicar un pago en borrador. Operative Oportunidad Pagado Pagos Presentación Producto Plantilla de producto Project Sub_Task Project Task Project Task Conf Propuesta Cotización Línea de pedido de venta Sub_Tareas Nuevo Task Created (%s): <a href=# data-oe-model=project.task data-oe-id=%d>%s</a> Tareas ¡El pago no se puede procesar porque la factura no está abierta! The product of the invoice is not configured in the project tasks. This task has been created from: <a href=# data-oe-model=sale.order data-oe-id=%d>%s</a> (%s) Type of Task You can't "Mark Done" this lead until %s No puedes editar el estado de esta iniciativa Debe definir una secuencia para %s en su empresa. Your next actions should flow efficiently: confirm the Brief
                to a Sale Order, then create the Invoice and collect the Payment. Nombre product.sub.task product.sub.task.name product.task.conf project.sub.task project.sub.task.conf project.task.conf sub.task.name 