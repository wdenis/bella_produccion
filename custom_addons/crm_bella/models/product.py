# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import date, time, datetime
from odoo import api, models, fields
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError

class ProductTemplate(models.Model):
    _inherit = "product.template"

    @api.model
    def create(self, vals):
        template = super(ProductTemplate, self).create(vals)
        if template:
            task = self.env['product.task.conf'].create({
                'name': 'Tasks',
                'product_id': template.id,
                'description': template.description_sale
            })
            if task:
                self.env['project.task.conf'].create({
                    'project_task_conf_id': task.id,
                    'name': task.product_id.name,
                    'planned_hours': 8,
                    'x_task_type': 'operative',
                    'days': 1
                })
        return template