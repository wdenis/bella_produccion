# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import date, time, datetime
from odoo import api, models, fields
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import mute_logger

class Product_task_conf(models.Model):
    _name = "product.task.conf"
    filter_2 = fields.Boolean('filter', compute='_compute_filter')
    filter = fields.Boolean('filter')
    name = fields.Char('NEW', default=lambda x: _('Tasks'))
    company_id = fields.Many2one('res.company')
    product_id = fields.Many2one('product.template', required=True, domain="[('company_id', '=',company_id)]" )
    project_task_conf = fields.One2many('project.task.conf', 'project_task_conf_id', string='Lines task')
    description = fields.Text('Description')

    @api.onchange('product_id')
    def _onchange_project_task(self):
        product_task_conf = self.env['product.task.conf'].search([('product_id','=',self.product_id.id)])
        if product_task_conf:
            raise UserError(_('The product is already created.'))
        if self.product_id:
            self.description = self.product_id.description_sale


    @api.onchange('name')
    def _onchange_name(self):
        if self:
            self.planned_hours = 8
            self.days = 1

    @api.onchange('company_id')
    def _onchange_company_id(self):
        if self:
            res_users = self.env['res.users'].browse(self._uid)
            self.company_id = res_users.company_id.id

    @api.one
    def load_tasks(self):
        product_id = self.env['product.template'].search([])
        for product_template in product_id:
            service_tracking = product_template.service_tracking
            product_task_conf = self.env['product.task.conf'].search([])
            if product_task_conf:
                for product_task_conf in product_task_conf:
                    if product_task_conf.product_id.id == product_template.id:
                        compare = True
                        break
                    else:
                        compare = False
            else:
                compare = True
            if compare != True and product_template.service_tracking == "task_new_project":
                task = self.create({
                        'name': 'Tasks',
                        'company_id' : product_template.company_id.id,
                        'product_id': product_template.id,
                        'description': product_template.description_sale
                })
                if task:
                    self.env['project.task.conf'].create({
                        'project_task_conf_id': task.id,
                        'name': product_template.name,
                        'planned_hours' : 8,
                        'x_task_type' : 'operative',
                        'days': 1
                    })

    @api.one
    @api.depends('filter_2')
    def _compute_filter(self):
        if self.company_id.id == self.write_uid.company_id.id:
            product_task_conf = self.env['product.task.conf'].search([('company_id','=',self.company_id.id)])
            product_task_conf.write({'filter': True})
            filter = self.env['product.task.conf'].search([('company_id', '!=', self.company_id.id)])
            filter.write({'filter': False})
        return True


class Project_task_conf(models.Model):
    _name = "project.task.conf"
    project_task_conf_id = fields.Integer('DNI')
    name = fields.Char('NEW',required=True)
    user_id = fields.Many2one('res.users', 'Assigned to')
    planned_hours = fields.Float(string='Initially Planned Hours',
                    help='Estimated time to do the task, usually set by the project manager when the task is in draft state.',required=True)
    x_task_type = fields.Selection([('operative', 'Operative')], 'Type of Task', default='operative', readonly=True,invisible=True)
    days = fields.Integer('Days task',required=True)
    description = fields.Text('Description')
    email_from = fields.Char("Email", size=128, help="These people will receive email.")


    @api.onchange('user_id')
    def _onchange_company_id(self):
        if self:
            self.email_from = self.user_id.email


class Project_sub_task_conf(models.Model):
    _name = "project.sub.task.conf"
    name = fields.Char('NEW', default=lambda x: _('Sub_Tasks'))
    project_task_conf_id = fields.Many2one('project.task.conf', required=True)
    project_sub_task_id = fields.One2many('project.sub.task', 'project_sub_task_id', string='Lines sub-task')


class Product_sub_task(models.Model):
    _name = 'project.sub.task'
    sale_ok = fields.Boolean(
        'Can be Sold', default=True,
        help="Specify if the product can be selected in a sales order line.")
    project_sub_task_id = fields.Integer('DNI')
    name = fields.Many2one('sub.task.name','Name',required=True)
    planned_hours = fields.Float(string='Initially Planned Hours',
                                 help='Estimated time to do the task, usually set by the project manager when the task is in draft state.',
                                 required=True,default=lambda x: _(8))
    days = fields.Integer('Days task', required=True)
    x_task_type = fields.Selection([('operative', 'Operative')], 'Type of Task',default='operative',readonly=True,invisible=True)
    description = fields.Text('Description')

    @api.onchange('name')
    def _compute_project_task(self):
        if self:
            self.planned_hours = 8
            self.days = 1

class Product_sub_task_name(models.Model):
    _name = 'sub.task.name'

    name = fields.Char('name')

class ProjectTask(models.Model):
    _inherit = "project.task"

    planned_hours_active = fields.Boolean('Active',default=False)
