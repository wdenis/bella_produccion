# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import date, time
from odoo import api, models, fields
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError
import time,locale
from datetime import datetime,timedelta

class Lead(models.Model):
    _inherit = "crm.lead"

    created = fields.Boolean('Created', default=False)
    target_brand = fields.Boolean('Target Brand', default=True)
    leads = fields.Boolean('Leads', default=False)
    prospects = fields.Boolean('Prospect', default=False)
    client = fields.Boolean('Client', default=False)
    paid = fields.Boolean('Paid', default=False)
    channel_leader = fields.Boolean('Channel Leader', compute='_compute_channel_leader')

    @api.depends('channel_leader')
    def _compute_channel_leader(self):
        if self.env['crm.team'].search([('user_id', '=', self.env.uid)]):
            self.channel_leader = True
        else:
            self._cr.execute('select * from res_groups_users_rel where uid = %s' % (self.env.uid))
            group_user = [group[0] for group in self._cr.fetchall()]
            if 20 in group_user:
                self.channel_leader = True

    @api.model
    def init_function_bella(self):
        ir_config_env = self.env['ir.config_parameter']
        for item in ir_config_env.search([('key', '=', 'crm.bella.target.brand')]).value.split(','):
            target_brand_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if target_brand_id:
                break
        for item in ir_config_env.search([('key', '=', 'crm.bella.lead')]).value.split(','):
            lead_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if lead_id:
                break
        for item in ir_config_env.search([('key', '=', 'crm.bella.prospect')]).value.split(','):
            prospect_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if prospect_id:
                break
        for item in ir_config_env.search([('key', '=', 'crm.bella.client')]).value.split(','):
            client_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if client_id:
                break
        for item in self.search([]):
            if not(item.created or item.target_brand or item.leads or item.prospects or item.client):
                if item.stage_id.id == target_brand_id:
                    sql = "UPDATE crm_lead SET created=TRUE, target_brand=TRUE where id=%s" %(item.id)
                    self._cr.execute(sql)
                elif item.stage_id.id == lead_id:
                    sql = "UPDATE crm_lead SET created=TRUE, leads=TRUE where id=%s" % (item.id)
                    self._cr.execute(sql)
                elif item.stage_id.id == prospect_id:
                    sql = "UPDATE crm_lead SET created=TRUE, prospects=TRUE where id=%s" % (item.id)
                    self._cr.execute(sql)
                elif item.stage_id.id == client_id:
                    sql = "UPDATE crm_lead SET created=TRUE, client=TRUE where id=%s" % (item.id)
                    self._cr.execute(sql)

    @api.multi
    def write(self, vals):
        if vals.get('stage_id', False) and not (vals.get('target_brand', False) or vals.get('leads', False) or vals.get('prospects', False) or vals.get('client', False)):
            raise UserError(_('You can\'t edit this lead stage.'))
        #if self.type == 'opportunity' and self.user_id.id != self._uid:
        #    raise UserError(_('You don\'t have permission to edit this lead. Only salesperson %s can edit it.' %(self.user_id.partner_id.display_name)))
        if self.ids and self.ids[0]:
            crm_id = self.ids[0]
        elif vals.get('id', False):
            crm_id = vals.get('id')
            del vals['id']
        if self.stage_id:
            stage_id = self.stage_id.id
        elif vals.get('stage_id'):
            stage_id = vals.get('stage_id')
        self._cr.execute("select value from ir_config_parameter where key = 'crm.bella.target.brand'")
        config_parameter =  self._cr.fetchall()[0][0]
        config_parameter = config_parameter.split(',')
        for item in config_parameter:
            target_brand_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if target_brand_id:
                break
        self._cr.execute("select value from ir_config_parameter where key = 'crm.bella.lead'")
        config_parameter = self._cr.fetchall()[0][0]
        config_parameter = config_parameter.split(',')
        for item in config_parameter:
            lead_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if lead_id:
                break
        self._cr.execute("select value from ir_config_parameter where key = 'crm.bella.prospect'")
        config_parameter = self._cr.fetchall()[0][0]
        config_parameter = config_parameter.split(',')
        for item in config_parameter:
            prospect_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if prospect_id:
                break
        self._cr.execute("select value from ir_config_parameter where key = 'crm.bella.client'")
        config_parameter = self._cr.fetchall()[0][0]
        config_parameter = config_parameter.split(',')
        for item in config_parameter:
            client_id = self.env['crm.stage'].search([('name', '=', item)]).id
            if client_id:
                break
        if not self.created and not vals.get('stage_id', False):
            sql = "UPDATE crm_lead SET user_id=NULL, team_id=NULL where id=%s" %(crm_id)
            self._cr.execute(sql)
        else:
            if stage_id == target_brand_id and (self.user_id or vals.get('user_id', False)) and (self.team_id or vals.get('team_id', False)):
                if vals.get('user_id', False) and vals['user_id'] != "" and vals.get('team_id', False) and vals['team_id'] != "":
                    sql = "UPDATE crm_lead SET stage_id=%s, target_brand=FALSE, leads=TRUE where id=%s" %(lead_id, crm_id)
                    self._cr.execute(sql)
            elif stage_id == lead_id and vals.get('stage_id', False):
                sql = "UPDATE crm_lead SET stage_id=%s, leads=FALSE, prospects=TRUE where id=%s" % (prospect_id, crm_id)
                self._cr.execute(sql)
            elif stage_id == prospect_id and vals.get('stage_id', False) and vals.get('paid', False):
                sql = "UPDATE crm_lead SET stage_id=%s, prospects=FALSE, client=TRUE, paid=TRUE where id=%s" % (client_id, crm_id)
                self._cr.execute(sql)
        super(Lead, self).write(vals)
        if self.leads and (not self.user_id or not self.team_id):
            sql = "UPDATE crm_lead SET stage_id=%s, leads=FALSE, target_brand=TRUE where id=%s" % (target_brand_id, crm_id)
            self._cr.execute(sql)
        return True

    @api.model
    def create(self, vals):
        new_target_brand = super(Lead, self).create(vals)
        if not new_target_brand.created:
            sql = "UPDATE crm_lead SET created=TRUE where id=%s" % (new_target_brand.id)
            self._cr.execute(sql)
        return new_target_brand

    @api.one
    def action_set_won_cancel(self):
        return True

    @api.one
    def action_set_lost_cancel(self):
        return True

    @api.one
    def sale_action_quotations_new_cancel(self):
        return True

class MailActivity(models.Model):
    _inherit = 'mail.activity'

    @api.multi
    def action_feedback(self, feedback=False):
        lead_env = self.env['crm.lead']
        self._cr.execute("select value from ir_config_parameter where key = 'crm.bella.mail.activity'")
        config_parameter = self._cr.fetchall()[0][0]
        config_parameter = config_parameter.split(',')
        activity = []
        lead_obj = lead_env.browse(self.res_id)
        for item in config_parameter:
            activity.append(item)
        if self.activity_type_id.name in activity:
            hoy = datetime.strptime(str(date.today()), '%Y-%m-%d')
            fecha = datetime.strptime(self.date_deadline, '%Y-%m-%d')
            if fecha <= hoy:
                self._cr.execute("select value from ir_config_parameter where key = 'crm.bella.lead'")
                parameter = self._cr.fetchall()[0][0]
                parameter = parameter.split(',')
                for item in parameter:
                    lead_id = self.env['crm.stage'].search([('name', '=', item)]).id
                    if lead_id:
                        break
                if lead_obj.stage_id.id == lead_id:
                    if lead_obj.partner_id and \
                        lead_obj.email_from and \
                        lead_obj.phone and \
                        lead_obj.user_id and \
                        lead_obj.team_id and \
                        lead_obj.date_deadline and \
                        lead_obj.description and \
                        lead_obj.partner_name and \
                        lead_obj.street and \
                        lead_obj.city and \
                        lead_obj.state_id and \
                        lead_obj.zip and \
                        lead_obj.country_id and \
                        lead_obj.contact_name and \
                        lead_obj.mobile:
                            lead_env.write({'stage_id':lead_id,
                                    'leads':False,
                                    'prospects':True,
                                    'id':lead_obj.id})
                    else:
                        return True
            else:
                raise UserError(_('You can\'t "Mark Done" this lead until %s' % (self.date_deadline)))
        message = self.env['mail.message']
        if feedback:
            self.write(dict(feedback=feedback))
        for activity in self:
            record = self.env[activity.res_model].browse(activity.res_id)
            record.message_post_with_view(
                'mail.message_activity_done',
                values={'activity': activity},
                subtype_id=self.env.ref('mail.mt_activities').id,
                mail_activity_type_id=activity.activity_type_id.id,
            )
            message |= record.message_ids[0]


        self.unlink()
        #return message.ids and message.ids[0] or False
        form_view = self.env.ref('crm.crm_case_form_view_oppor')
        tree_view = self.env.ref('crm.crm_case_tree_view_oppor')
        return {
            'name': _('Opportunity'),
            'view_type': 'form',
            'view_mode': 'tree, form',
            'res_model': 'crm.lead',
            'domain': [('type', '=', 'opportunity')],
            'res_id': lead_obj.id,
            'view_id': False,
            'views': [
                (form_view.id, 'form'),
                (tree_view.id, 'tree'),
                (False, 'kanban'),
                (False, 'calendar'),
                (False, 'graph')
            ],
            'type': 'ir.actions.act_window',
            'context': {'default_type': 'opportunity'}
        }

class SaleOrder(models.Model):
    _inherit = "sale.order"

    brief_id = fields.Many2one('sales.brief', 'Brief')
    brief_active = fields.Boolean('Brief_active',default=False)

    @api.onchange('partner_id')
    def _brief_domain(self):
        if self._context.get('brief_active',False):
                self.brief_active = True
        if self.partner_id:
            return {'domain': {'brief_id': [('partner_id.id', '=', self.partner_id.id)]}}





class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    date_task = fields.Date('Date',default=time.strftime('%d-%m-%y'))

    def _timesheet_find_project(self):
        self.ensure_one()
        Project = self.env['project.project']
        project = self.product_id.with_context(force_company=self.company_id.id).project_id
        if not project:
            # find the project corresponding to the analytic account of the sales order
            account = self.order_id.analytic_account_id
            if not account:
                self.order_id._create_analytic_account(prefix=self.product_id.default_code or None)
                account = self.order_id.analytic_account_id
            project = Project.search([('analytic_account_id', '=', account.id)], limit=1)
            if not project:
                project_name = '%s (%s)' % (
                account.name, self.order_partner_id.ref) if self.order_partner_id.ref else account.name
                project = Project.create({
                    'name': project_name,
                    'user_id':self.order_id.opportunity_id.user_id.id,
                    'allow_timesheets' : self.product_id.service_type == 'timesheet',
                    'analytic_account_id' : account.id,
                    'x_deliverables' : self.order_id.brief_id.x_deliverables,
                    'x_objetive' : self.order_id.brief_id.x_objetive,
                    'x_scope' : self.order_id.brief_id.x_scope,
                    'x_project_type' : 'external',
                    'privacy_visibility' : 'followers',
                    'alias_name': self.order_id.opportunity_id.user_id.email,
                    'date_start' : self.date_task,
                })
                # set the SO line origin if product should create project
                if not project.sale_line_id and self.product_id.service_tracking in ['task_new_project',
                                                                              'project_only']:
                    project.write({'sale_line_id': self.id})
        return project

    def _timesheet_create_task_prepare_values(self,lines_product_task_id):
        self.ensure_one()
        task = self.env['project.task.conf']
        task = task.search([('id','=',lines_product_task_id)])
        project = self._timesheet_find_project()
        if self.product_id.categ_id.name == 'Staffing' or self.product_id.categ_id.name == 'staffing':
            planned_hours = self.hours_per_day * self.total_days
        else:
            planned_hours = task.planned_hours
        #planned_hours = self._convert_qty_company_hours()
        #'%s:%s' % (self.order_id.name or '', self.name.split('\n')[0] or self.product_id.name)
        return {
            'name': str(task.name) or " ",
            'planned_hours_active' : True,
            'planned_hours': planned_hours,
            'remaining_hours': planned_hours,
            'partner_id': self.order_id.partner_id.id,
            'description': str(task.description) or " ",
            'project_id': project.id,
            'sale_line_id': self.id,
            'company_id': self.company_id.id,
            'email_from': str(task.email_from) or " ",
            'user_id': task.user_id.id,# force non assigned task, as created as sudo()
            'x_task_type' : task.x_task_type,
            'days': task.days,
            'date_assign' : self.date_task,
            'date_deadline': self.planned_days(lines_product_task_id,vals=True),

        }

    @api.multi
    def _timesheet_create_task(self):
        """ Generate task for the given so line, and link it.

            :return a mapping with the so line id and its linked task
            :rtype dict
        """

        product_task = self.env['product.task.conf']
        result = {}
        for so_line in self:
            # create task
            product_task_id = product_task.search([('product_id', '=', so_line.product_id.product_tmpl_id.id)])
            if product_task_id:
                for lines_product_task_id in product_task_id.project_task_conf:
                    values = so_line._timesheet_create_task_prepare_values(lines_product_task_id.id)
                    task = self.env['project.task'].sudo().create(values)
                    so_line.write({'task_id': task.id})
                    # post message on SO
                    msg_body = _("Task Created (%s): <a href=# data-oe-model=project.task data-oe-id=%d>%s</a>") % (
                        so_line.product_id.name, task.id, task.name)
                    so_line.order_id.message_post(body=msg_body)
                    # post message on task
                    task_msg = _(
                        "This task has been created from: <a href=# data-oe-model=sale.order data-oe-id=%d>%s</a> (%s)") % (
                               so_line.order_id.id, so_line.order_id.name, so_line.product_id.name)
                    task.message_post(body=task_msg)
                    so_line._create_sub_task(lines_product_task_id.id,task)
                result[so_line.id] = task
            else:
                raise UserError(_('The product of the invoice is not configured in the project tasks.'))
        return result


    @api.multi
    def _create_sub_task(self, lines_product_task_id,task):
        """ For service lines, create the task or the project. If already exists, it simply links
            the existing one to the line.
        """
        self.ensure_one()
        sub_task = self.env['project.sub.task.conf']
        sub_task = sub_task.search([('project_task_conf_id', '=', lines_product_task_id)])
        project = self._timesheet_find_project()
        parent_id = task.id
        project_id =  project.id
        for project_sub_task in sub_task.project_sub_task_id:
            values = {
                'name' : project_sub_task.name.name,
                'planned_hours': project_sub_task.planned_hours,
                'remaining_hours': project_sub_task.planned_hours,
                'partner_id': self.order_id.partner_id.id,
                'parent_id':parent_id,
                'description': str(project_sub_task.description) or " ",
                'project_id': project_id,
                'sale_line_id': self.id,
                'company_id': self.company_id.id,
                'user_id': False,# force non assigned task, as created as sudo()
                'x_task_type' : project_sub_task.x_task_type,
                'date_deadline': self.planned_days(project_sub_task.id,vals=False)
                }
            self.env['project.task'].sudo().create(values)

    @api.multi
    def planned_days(self,lines_product_task_id,vals):
        if vals == True:
            task = self.env['project.task.conf']
            task = task.search([('id', '=', lines_product_task_id)])
            date_start = datetime.now()
            if self.product_id.categ_id.name == 'Staffing' or self.product_id.categ_id.name == 'staffing':
                days = self.hours_per_day * self.total_days
                sum_days = timedelta(days=int(days))
            else:
                sum_days = timedelta(days=task.days)
            date_final = date_start + sum_days
            date_deadline = date_final
            for d in range((date_final - date_start).days+1):
                d = d
                date_sum = timedelta(days=d)
                date_sum = date_start + date_sum
                date = date_sum.strftime('%d-%m-%Y')
                week_days = datetime.strptime(date, '%d-%m-%Y').strftime('%A')
                if week_days == 'sábado' or week_days == 'domingo':
                    date_su = timedelta(days=1)
                    date_deadline = date_deadline + date_su
        else:
            task = self.env['project.sub.task']
            task = task.search([('id', '=', lines_product_task_id)])
            date_start = datetime.now()
            sum_days = timedelta(days=task.days)
            date_final = date_start + sum_days
            date_deadline = date_final
            for d in range((date_final - date_start).days + 1):
                d = d
                date_sum = timedelta(days=d)
                date_sum = date_start + date_sum
                date = date_sum.strftime('%d-%m-%Y')
                week_days = datetime.strptime(date, '%d-%m-%Y').strftime('%A')
                if week_days == 'sábado' or week_days == 'domingo':
                    date_su = timedelta(days=1)
                    date_deadline = date_deadline + date_su
        return date_deadline

    @api.multi
    def _timesheet_service_generation(self, paid=False):
        """ For service lines, create the task or the project. If already exists, it simply links
            the existing one to the line.
        """
        if paid:
            for so_line in self.filtered(lambda sol: sol.is_service):
                # create task
                if so_line.product_id.service_tracking == 'task_global_project':
                    so_line._timesheet_find_task()
                # create project
                if so_line.product_id.service_tracking == 'project_only':
                    so_line._timesheet_find_project()
                # create project and task
                if so_line.product_id.service_tracking == 'task_new_project':
                    so_line._timesheet_find_task()

class account_payment(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:

            if rec.state != 'draft':
                raise UserError(_("Only a draft payment can be posted."))

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            # Use the right sequence to set the name
            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if rec.partner_type == 'customer':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if rec.partner_type == 'supplier':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
            rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(
                sequence_code)
            if not rec.name and rec.payment_type != 'transfer':
                raise UserError(_("You have to define a sequence for %s in your company.") % (sequence_code,))

            # Create the journal entry
            amount = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            move = rec._create_payment_entry(amount)

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(
                    lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()

            rec.write({'state': 'posted', 'move_name': move.name})

            sale_order_env = self.env['sale.order']
            crm_lead_env = self.env['crm.lead']
            ir_config_env = self.env['ir.config_parameter']
            for item in ir_config_env.search([('key', '=', 'crm.bella.prospect')]).value.split(','):
                prospect_id = self.env['crm.stage'].search([('name', '=', item)]).id
                if prospect_id:
                    break
            for invoice in self.invoice_ids:
                quotation_obj = sale_order_env.search([('name','=',invoice.origin)])
                opportunity_obj = crm_lead_env.browse(quotation_obj.opportunity_id.id)
                if opportunity_obj.stage_id.id == prospect_id and not opportunity_obj.paid:
                    for item in ir_config_env.search([('key', '=', 'crm.bella.client')]).value.split(','):
                        client_id = self.env['crm.stage'].search([('name', '=', item)]).id
                        if client_id:
                            break
                    crm_lead_env.write({'stage_id': prospect_id,
                                    'prospects': False,
                                    'client': True,
                                    'id': opportunity_obj.id,
                                    'paid': True})
                    quotation_obj.order_line._timesheet_service_generation(paid=True)




