# -*- coding: utf-8 -*-

from itertools import groupby
from odoo import api, fields, models, _

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    partner_invoice_id = fields.Many2one(
    	'res.partner', 
    	string='Invoice Address', 
    	readonly=True, 
    	states={'draft': [('readonly', False)], 
    	'sent': [('readonly', False)]}, 
    	help="Invoice address for current invoice.")


    @api.onchange('partner_id', 'company_id')
    def _onchange_invoice_address(self):      
        addr = self.partner_id.address_get(['invoice'])      
        self.partner_invoice_id = addr and addr.get('invoice')

    @api.multi
    def get_delivery_partner_id(self):
        self.ensure_one()
        return self.partner_invoice_id.id or super(AccountInvoice, self).get_delivery_partner_id()

    def _get_refund_common_fields(self):
        return super(AccountInvoice, self)._get_refund_common_fields() + ['partner_invoice_id']


    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            if 'company_id' in vals:
                vals['name'] = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code('sale.order') or _('New')
            else:
                vals['name'] = self.env['ir.sequence'].next_by_code('sale.order') or _('New')

        # Makes sure partner_invoice_id', 'partner_shipping_id' and 'pricelist_id' are defined
        if any(f not in vals for f in ['partner_invoice_id']):
            partner = self.env['res.partner'].browse(vals.get('partner_id'))
            addr = partner.address_get(['delivery', 'invoice'])
            vals['partner_invoice_id'] = vals.setdefault('partner_invoice_id', addr['invoice'])
            
        result = super(AccountInvoice, self).create(vals)
        return result