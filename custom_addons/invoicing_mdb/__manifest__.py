# -*- coding: utf-8 -*-
{
    'name': "invoicing_mdb",

    'summary': """
        Invoice Client Mdb""",

    'description': """
        Invoice Client Mdb
    """,

    'author': "Rafael E. Requena G.",
    'website': "https://bella.markdebrand.com",
    'category': 'Project',
    'version': '0.1',
    'depends': ['base','sale','account'],
    'data': [
        'data/report_paperformat.xml',
        'report/sale_report.xml',
        'views/report_invoice.xml',

    ],    
    'demo': [
    
    ],
}