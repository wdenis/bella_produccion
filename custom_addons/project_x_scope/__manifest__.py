# -*- coding: utf-8 -*-
{
    'name': 'Project  x_scope',
    'category': 'Extra Tools',
    'summary': 'Add scope on project',
    'version': '11.0.1.0.0',
    'author': 'MDB',
    'depends': [
        'base_setup',
         'project'
        ],
    'data': [
        'views/project_project.xml',
        ],
    'installable': True,
}
