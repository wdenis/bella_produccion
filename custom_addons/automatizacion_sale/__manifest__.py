# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# 


{
    'name': 'sales_automation',
    'version': '1.1',
    'category': 'Sale',
    'author': 'William Denis',
    'maintainer': 'wdenis123.1@gmail.com',
    'description': """

    Automatization of sales when accepting a quote sends you an email with the pro-form   

    """,
    'depends': ['base', 'sale_management'],
    'data': [],
}
