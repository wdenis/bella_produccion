# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Bella Invoice Control Number',
    'version' : '1.0',
    'summary': 'Add control number to invoice',
    'sequence': 30,
    'description': """
Add control number to invoice
    """,
    'category': 'Account',
    'depends' : ['base',
                 'account'],
    'data': [
        'views/account_invoice.xml',
        'data/account_invoice_data.xml'
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}