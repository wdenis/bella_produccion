# -*- coding: utf-8 -*-

from odoo import api, models, fields, _

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    control_number = fields.Char('Control Number', readonly=True, states={'draft': [('readonly', False)]})
    active_control_number = fields.Boolean('Active Control Number', default=False, compute='_compute_active_control_number')

    @api.one
    def test(self):
        return True

    api.depends('active_control_number')
    def _compute_active_control_number(self):
        context = self._context.copy()
        if context.get('control_number',False):
            del context['control_number']
        self._cr.execute("select value from ir_config_parameter where key = 'account.invoice.control.number'")
        active_companies = self._cr.fetchall()[0][0]
        active_companies = active_companies.split(',')
        active_user = self.env['res.users'].search([('id','=',self.env.uid)])
        for company in active_companies:
            if active_user.company_id.name == company:
                break
            for line in self:
                line.active_control_number = True
                #line.texto = self._context
                #self_updated = self.with_context(control_number=True)
                # current context is {'key1': True}
                #r2 = records.with_context({}, key2=True)
                # -> r2._context is {'key2': True}
                #r2 = records.with_context(key2=True)
                # -> r2._context is {'key1': True, 'key2': True}
            #self_updated = self.with_context(control_number=True)
            #context.update({'active_control_number': True})
            #res = super(AccountInvoice,self_updated).fields_get()
            #return res