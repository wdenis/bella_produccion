# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* bella_quotation_template
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 11.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-05 01:49+0000\n"
"PO-Revision-Date: 2018-06-05 01:49+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "&amp;bull;"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "&amp;nbsp;<span>on</span>&amp;nbsp;"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "<span>Pro-Forma Invoice # </span>"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "<strong>Fiscal Position Remark:</strong>"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "<strong>Sub-total</strong>"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "<strong>Subtotal: </strong>"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Amount"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Bill to"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Code"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Date"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Description"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Disc.(%)"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Exp. Date"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Order #"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Payment terms"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "QTY"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Quotation #"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Salesperson"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Ship to"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Taxes"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Total Price"
msgstr ""

#. module: bella_quotation_template
#: model:ir.ui.view,arch_db:bella_quotation_template.bella_report_saleorder_document
msgid "Unit Price"
msgstr ""

