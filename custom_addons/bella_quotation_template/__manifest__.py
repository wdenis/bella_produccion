# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Bella Quotation Template',
    'version' : '1.0',
    'summary': 'Bella template for quotations',
    'sequence': 30,
    'description': """
Bella template for quotation
    """,
    'category': 'Sales',
    'depends' : ['base',
                 'sale'],
    'data': [
        'views/sale_report_templates.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}