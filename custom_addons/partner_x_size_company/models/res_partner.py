# -*- coding: utf-8 -*-

from odoo import fields,models


class ResPartner(models.Model):

    _inherit = 'res.partner'


    x_size_company = fields.Selection([('microenterprise','Microenterprise'),('small','Small company'),('medium','Medium Company'),('big','Big company')],'Size Company')
