# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# 


{
    'name': 'TargetBrand',
    'version': '1.1',
    'category': 'Target Brand',
    'author': 'William Denis',
    'maintainer': 'wdenis123.1@gmail.com',
    'description': """

    Improvements and changes in the crm a target view with their default values

   

    """,
    'depends': ['base'],
    'data': [
        'views/res_partner.xml'
    ],
}
