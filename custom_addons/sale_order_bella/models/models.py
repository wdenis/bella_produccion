# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp
from decimal import *

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	quota = fields.Integer(string='Quota', translate=True)

	@api.one
	def format_amount(self, amount, currency):
		split_amount = str(amount).split('.')
		decimal_separator = '.'
		if len(split_amount) == 2:
			int_part = format(int(split_amount[0]), ',d')
			if currency == 'VEF' or currency == 'COP':
				int_part = int_part.replace(',','.')
				decimal_separator = ','
			dec_part = split_amount[1]
			if len(dec_part) < 2:
				dec_part = dec_part + '0'
			new_amount = int_part + decimal_separator + dec_part
			return new_amount
		else:
			amount = format(int(amount), ',d')
			if currency == 'VEF' or currency == 'COP':
				amount = amount.replace(',','.')
				decimal_separator = ','
			return amount + decimal_separator + '00'

	@api.one
	def get_translation(self, amount_total):
		if self.payment_term_id.id == 1 or \
		   self.payment_term_id.id == 2 or \
		   self.payment_term_id.id == 3 or \
		   self.payment_term_id.id == 6:
			if self.payment_term_id.id == 6:
				amount = amount_total
				amount_total = amount - (amount_total * 0.50)
			amount_total = Decimal(str(amount_total)).quantize(Decimal('.01'), rounding=ROUND_UP)
			amount_total = self.format_amount(amount_total, self.currency_id.name)[0]
			note = _("The customer chose the payment term ") + self.payment_term_id.name + _(" and must pay ") + self.currency_id.symbol + " " + str(amount_total) + ".\n\n\n"
		elif self.payment_term_id.id == 7:
			amount = amount_total
			amount_total = amount - (amount_total * 0.50)
			note = _("The customer chose the payment term ") + self.payment_term_id.name + _(", must pay two installments of ") + self.currency_id.symbol + " " + str(amount_total / 2) + _(" correspondiente al 25%.\n\n\n")
		elif self.payment_term_id.id == 8:
			amount = amount_total
			amount_total = amount - (amount_total * (self.quota / 100))
			note = _("The customer chose the payment term ") + self.payment_term_id.name + _(" and he must quotas pay of ") + str(self.quota) + "%.\n\n\n"
		else:
			note = ""
		return note

	@api.onchange('order_line', 'payment_term_id')
	def onchange_value_payment_term(self):

		if self.payment_term_id.name == False:
			self.note = 'MARKDEBRAND'
			return False
		else:
			nota = 'MARKDEBRAND'
			if self.partner_id:
				language = self.partner_id.lang
			else:
				language = 'en_US'
			if self.env['ir.config_parameter'].sudo().get_param('sale.use_sale_note') and self.env.user.company_id.sale_note:
				nota = self.with_context(lang=self.partner_id.lang).env.user.company_id.sale_note
			self.note = self.with_context(lang=language).get_translation(self.amount_total)[0] + nota
