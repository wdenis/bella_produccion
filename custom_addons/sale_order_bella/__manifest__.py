# -*- coding: utf-8 -*-
{
    'name': "sale_order_bella",

    'summary': """
        """,

    'description': """
        Este modulo extiende del modelo sale_order y le agrega la funcionalidad pago a largo plazo para los clientes de Markdebrand
    """,

    'author': "Jose Nicolielly",
    'website': "https://markdebrand.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Bella Project',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/account_data_bella.xml',
        'views/views.xml',
    ],
}