# -*- encoding: utf-8 -*-
{
    'name': 'Custom do not have an account',
    'category': 'Web',
    'description': 'Remove do not have an account in the website',
'summary': 'Remove do not have an account in the website',
    'version': '11.0.1.0.0',
    'author': 'MDB',
    'depends': ['web'],
    'data': [
        'view/template.xml'
    ],
   
}
