# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ResPartner(models.Model):
    _inherit = 'res.partner'
    _description = 'Add fields cod'

    cod = fields.Char(
    	string='Cod Client',
    	size=64,
    	required=False,
    	readonly=True    	
    	)


    @api.model
    def create(self, vals):
        if vals.get('name'):
            if 'name' in vals:
                vals['cod'] = self.env['ir.sequence'].next_by_code('res.partner') or _('New')
            
        result = super(ResPartner, self).create(vals)
        return result



    @api.multi
    def write(self, vals):
        if vals.get('name'):
            if 'name' in vals:
                vals['cod'] = self.env['ir.sequence'].next_by_code('res.partner') or _('New')
            
        result = super(ResPartner, self).write(vals)
        return result

