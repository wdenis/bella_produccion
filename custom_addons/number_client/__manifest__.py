# -*- coding: utf-8 -*-
{
    'name': "number_client",
    'summary': """
        Numbering of clients""",
    'description': """
        Creation of sequential number of clients
    """,
    'author': "Rafael E. Requena G.",
    'website': "https://bella.markdebrand.com",
    'category': 'Sales',
    'version': '0.1',
    'depends': ['base','sale'],
    'data': [
        'views/res_partner_view.xml',
        'data/ir_sequence_data.xml'
    ],
    'demo': [        
    ],
    'installable': True,
    'auto_install': False,
}