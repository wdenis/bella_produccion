# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# 


{
    'name': 'project_view',
    'version': '1.1',
    'category': 'Project',
    'author': 'William Denis',
    'maintainer': 'wdenis123.1@gmail.com',
    'description': """

    Project view improvements to show by default the view without children tasks and filter to improve the user experience   

    """,
    'depends': ['base', 'project'],
    'data': [
        'views/project_views.xml'
    ],
}
