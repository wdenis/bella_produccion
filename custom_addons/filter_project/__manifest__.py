# -*- coding: utf-8 -*-
{
    'name': "filter_project",

    'summary': """
        Filter by project in session""",

    'description': """
        Filter by project in session
    """,

    'author': "Rafael E. Requena G.",
    'website': "https://bella.markdebrand.com",
    'category': 'Project',
    'version': '0.1',
    'depends': ['base','project'],
    'data': [
        'views/project_views.xml',   
    ],    
    'demo': [
    
    ],
}