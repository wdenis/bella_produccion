# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Task(models.Model):
 	_inherit = 'project.task'

 	date_deadline = fields.Datetime(string='Deadline', index=True, copy=False, required=True, default=fields.Datetime.now)
 	date_start = fields.Datetime(string='Starting Date', index=True, copy=False, required=True)