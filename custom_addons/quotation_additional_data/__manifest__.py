# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Quotation Additional Data',
    'version' : '1.0',
    'summary': 'Include Price per Hour, Hours per Day, Total Days and Price per Day fields',
    'sequence': 30,
    'description': """
Include additional fields on quotations
    """,
    'category': 'Sales',
    'depends' : ['base',
                 'sale'],
    'data': [
        'views/sale_views.xml',
        'data/quotation_config_data.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
