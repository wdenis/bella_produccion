# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import uuid

from itertools import groupby
from datetime import datetime, timedelta
from werkzeug.urls import url_encode

from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError
from odoo.osv import expression
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

from odoo.tools.misc import formatLang

from odoo.addons import decimal_precision as dp

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    active_fields = fields.Boolean(string='Quotations for Events', default=False)
    active_company = fields.Boolean(compute='_active_company')

    @api.depends('company_id')
    def _active_company(self):
        options = self.env['ir.config_parameter'].search([('key','=','quotation.config.template')])
        if self.company_id.name in options.value.split(','):
            self.active_company = True
        else:
            self.active_fields = False
            self.active_company = False

    @api.multi
    def _get_tax_amount_by_group(self):
        self.ensure_one()
        res = {}
        for line in self.order_line:
            if line.product_id and line.product_id.product_tmpl_id.categ_id.name == "Staffing":
                price_reduce = ((line.price_unit * line.hours_per_day) * line.total_days) * (1.0 - line.discount / 100.0)
            else:
                price_reduce = line.price_unit * (1.0 - line.discount / 100.0)
            taxes = line.tax_id.compute_all(price_reduce, quantity=line.product_uom_qty, product=line.product_id,
                                            partner=self.partner_shipping_id)['taxes']
            for tax in line.tax_id:
                group = tax.tax_group_id
                res.setdefault(group, {'amount': 0.0, 'base': 0.0})
                for t in taxes:
                    if t['id'] == tax.id or t['id'] in tax.children_tax_ids.ids:
                        res[group]['amount'] += t['amount']
                        res[group]['base'] += t['base']
        res = sorted(res.items(), key=lambda l: l[0].sequence)
        res = [(l[0].name, l[1]['amount'], l[1]['base'], len(res)) for l in res]
        return res


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id', 'hours_per_day', 'total_days')
    def _compute_amount(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            if line.product_id and line.product_id.product_tmpl_id.categ_id.name == "Staffing":
                price = ((line.price_unit * line.hours_per_day) * line.total_days) * (1 - (line.discount or 0.0) / 100.0)
            else:
                line.hours_per_day = 0
                line.total_days = 0
                price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty,
                                            product=line.product_id, partner=line.order_id.partner_shipping_id)
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    @api.depends('price_per_hour', 'hours_per_day')
    def _compute_price_per_day(self):
        """
        Compute the amounts of the SO line.
        """
        return True
        #for line in self:
            #pass
            #line.price_per_day = line.price_per_hour * line.hours_per_day
            #line.price_per_day = 1 * 1

    #price_per_hour = fields.Float('Price per Hour', digits=dp.get_precision('Product Price'), default=0.0)
    hours_per_day = fields.Float('Hours per Day', digits=dp.get_precision('Product Price'), default=0.0)
    total_days = fields.Float('Total Days')
    price_per_day = fields.Float(string='Price per Day', digits=dp.get_precision('Product Price'), default=0.0, readonly=True)

    @api.multi
    def _prepare_invoice_line(self, qty):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        """
        self.ensure_one()
        res = {}
        account = self.product_id.property_account_income_id or self.product_id.categ_id.property_account_income_categ_id
        if not account:
            raise UserError(
                _('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') %
                (self.product_id.name, self.product_id.id, self.product_id.categ_id.name))

        fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
        if fpos:
            account = fpos.map_account(account)

        res = {
            'name': self.name,
            'sequence': self.sequence,
            'origin': self.order_id.name,
            'account_id': account.id,
            'price_unit': self.price_unit,
            'quantity': qty,
            'hours_per_day': self.hours_per_day,
            'total_days': self.total_days,
            'discount': self.discount,
            'uom_id': self.product_uom.id,
            'product_id': self.product_id.id or False,
            'layout_category_id': self.layout_category_id and self.layout_category_id.id or False,
            'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],
            'account_analytic_id': self.order_id.analytic_account_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
        }
        return res

class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity',
                 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
                 'invoice_id.date_invoice','hours_per_day','total_days')
    def _compute_price(self):
        currency = self.invoice_id and self.invoice_id.currency_id or None
        if self.product_id and self.product_id.product_tmpl_id.categ_id.name == "Staffing":
            price = ((self.price_unit * self.hours_per_day) * self.total_days) * (1 - (self.discount or 0.0) / 100.0)
        else:
            self.hours_per_day = 0
            self.total_days = 0
            price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        taxes = False
        if self.invoice_line_tax_ids:
            taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id,
                                                          partner=self.invoice_id.partner_id)
        self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price
        self.price_total = taxes['total_included'] if taxes else self.price_subtotal
        if self.invoice_id.currency_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
            price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id.date_invoice).compute(
                price_subtotal_signed, self.invoice_id.company_id.currency_id)
        sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
        self.price_subtotal_signed = price_subtotal_signed * sign

    hours_per_day = fields.Float('Hours per Day', default=0.0)
    total_days = fields.Float('Total Days', default=0.0)