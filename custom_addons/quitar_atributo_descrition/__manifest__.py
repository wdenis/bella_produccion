# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# 


{
    'name': 'remove_product_attribute',
    'version': '1.1',
    'category': 'Sale',
    'author': 'William Denis',
    'maintainer': 'wdenis123.1@gmail.com',
    'description': """

    Remove attribute of the product from the description in the quotation, invoice and pro-form   

    """,
    'depends': ['base', 'sale_management'],
    'data': [],
}
