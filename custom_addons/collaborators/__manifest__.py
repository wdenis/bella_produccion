# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

# 


{
    'name': 'collaborators',
    'version': '1.1',
    'category': 'hr',
    'author': 'William Denis',
    'maintainer': 'wdenis123.1@gmail.com',
    'description': """

    Modification of Contributors module add field for personal mail   

    """,
    'depends': ['base', 'hr'],
    'data': [
        'views/hr_views.xml'
    ],
}
