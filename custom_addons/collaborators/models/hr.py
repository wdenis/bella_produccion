# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class Employee(models.Model):
    _inherit = 'hr.employee'

    personal_email1 = fields.Char('Personal Email')
    bank_account_type = fields.Selection([
        ('current', 'Current'),
        ('saving', 'Saving')
    ], groups="hr.group_hr_user", string='Type of Bank Account')
    contact_person = fields.Char('Contact Person')
    contact_address = fields.Char('Contact Address')
    contact_phone = fields.Char('Contact Phone')
    employee_address = fields.Char('Employee Address')
    is_a_seller = fields.Boolean(string='Is a Seller', help="Check this box if you are a contributor.")
