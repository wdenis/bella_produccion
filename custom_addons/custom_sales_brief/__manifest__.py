# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Generation of Brief',
    'version' : '1.0',
    'summary': 'Indispensable requirement to generate service and Staffing quotes',
    'description': """
The brief is an indispensable requirement to generate service and staffing quotes, both in the sales module and in the CRM
    """,
    'category': 'sales',
    'depends' : [
        'base',
        'base_setup',
        'account',
        'sales_team',
        'mail',
        'fetchmail',
        'contacts',
        ],
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'views/sales_brief_views.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
