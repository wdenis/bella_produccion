# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api,_
import time

class Brief(models.Model):
    _name = 'sales.brief'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']
    _mail_post_access = 'read'

    name =fields.Char ('NEW',default=lambda x: _('New'))
    partner_id = fields.Many2one('res.partner', string='Customer',required="1")
    date = fields.Date('Date',default=time.strftime('%Y-%m-%d'))
    company_information = fields.Text('History / Company Information')
    company_information_brand = fields.Text('History / Brand Information')
    company_name_brand = fields.Char('Name of the brand')
    company_time_market = fields.Char('Time in the market')
    company_objectives = fields.Text('Objectives',help="Objectives of the brand in the medium and short term")
    company_positioning = fields.Integer('Positioning of the brand')
    company_structure = fields.Text('Organizational Structure')
    company_reason = fields.Char('Reason Why')
    company_Selling = fields.Char('Selling Line')
    company_dofa = fields.Text('DOFA Analysis')
    company_competition = fields.Char('Competition')
    company_Difference = fields.Text('Difference vs the competition')
    company_target = fields.Char('Target audiences')
    company_location = fields.Char('Geographic location',help="Geographic location of the brand / services")
    company_country = fields.Many2one('res.country','Country')
    company_city = fields.Char('City')
    company_state = fields.Many2one('res.country.state','State')
    note_Additional = fields.Text('Additional note')
    product_line_ids = fields.One2many('sales.brief.product', 'id_sales_brief_product', string='Product Lines')
    message_follower_ids = fields.One2many(
        'mail.followers', 'res_id', string='Followers')
    activity_ids = fields.One2many(
        'mail.activity', 'res_id', 'Activities',
        auto_join=True,
        groups="base.group_user")
    message_ids = fields.One2many('mail.message', 'res_id', string='Messages',
        auto_join=True,groups="base.group_user")
    x_deliverables = fields.Text('Deliverables',required=True)
    x_objetive = fields.Text('Objetive',required=True)
    x_scope = fields.Text('Scope',required=True)

    @api.model
    def create(self, vals):
        if not vals.get('name') or vals['name'] == _('New'):
            vals['name'] = self.env['ir.sequence'].next_by_code('sale.brief.sequence') or _('New')
        return super(Brief, self).create(vals)

class Brief_product(models.Model):
    _name = 'sales.brief.product'
    id_sales_brief_product = fields.Integer("DNI")
    brief_id = fields.Many2one('sales.brief')
    product_id = fields.Many2one('product.template', required=True)
    description_sale = fields.Text(string='Description',required="1")
    category = fields.Char(string='Category')
    salesperson_note = fields.Text('Salesperson Note')
    service_client_note = fields.Text('Service Client Note')


    @api.onchange('product_id')
    def _compute_brief_description(self):
        if self:
            self.description_sale = self.product_id.description_sale
            self.category = self.product_id.categ_id.name

class MailActivity(models.Model):
    _inherit = "mail.activity"

class Followers(models.Model):
    _inherit = "mail.followers"

class MailMessage(models.Model):
    _inherit = 'mail.message'
