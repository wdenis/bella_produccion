��    >        S   �      H  
   I     T     ]     m     s     |     �     �  -   �  
   �  
   �     �     �     �     �     �               0     =  	   P     Z  +   n     �     �     �     �     �     �                         /  z   3     �  
   �  4   �     �               6     >     L  
   T     _     p     v     �     �     �     �     �  �   �     ^	     r	     �	     �	     �	     �	     �	  C  �	     *  	   6     @     O  
   U     `     g     s  -   y  
   �  	   �     �     �     �     �     �     �     �          $  
   =     H  ,   _  #   �  %   �     �     �     �          &     .     7     ;     N  z   R     �  	   �  2   �                :     V     _     t     }     �     �     �  &   �     �     �     �       �        �     �     �     �                     /   '   ,      5             &          7                 6              .           3   $           #          	   %         :                                  >       (   4   +   "   
   ;              =      <   1   *   )                     2                           0   !   9   -   8           Activities Activity Additional note Brief Category City Competition Country Create a Brief, the first step of a new sale. Created by Created on Customer DNI DOFA Analysis Date Deliverables Description Difference vs the competition Display Name Document Followers Followers Geographic location Geographic location of the brand / services History / Brand Information History / Company Information ID Last Modified on Last Updated by Last Updated on Message Messages NEW Name of the brand New Note that once a Brief becomes a Sale Order, it will be moved
                from the Brief list to the Sales Order list. Number Brief Objectives Objectives of the brand in the medium and short term Objetive Organizational Structure Positioning of the brand Product Product Lines Project Reason Why Salesperson Note Scope Selling Line Service Client Note State Target Market Target audiences Time in the market Your next actions should flow efficiently: confirm the Brief
                to a Sale Order, then create the Invoice and collect the Payment. mail.activity.brief mail.activity.sale mail.activity.sale.brief mail.followers.brief mail.message.brief sales.brief sales.brief.product Project-Id-Version: Odoo Server 11.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-08-15 15:47+0000
PO-Revision-Date: 2018-08-15 11:50-0400
Last-Translator: <>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: es
X-Generator: Poedit 1.8.7.1
 Actividades Actividad Nota adicional Brief Categoría Ciudad Competencia País Create a Brief, the first step of a new sale. Creado por Creado el Cliente DNI Análisis DOFA Fecha Entregables Descripción Difference vs the competition Nombre mostrado Seguidores del documento Seguidores Ubicación Geográfica Ubicación Geográfica de la marca/servicios Historia / Información de la Marca Historia / Información de la empresa ID Última Modificación en Última actualización de Última Actualización el Mensaje Mensajes NEW Nombre de la marca New Note that once a Brief becomes a Sale Order, it will be moved
                from the Brief list to the Sales Order list. Number Brief Objetivos Objetivos de la marca en el mediano y corto plazo: Objetive Estructura Organizacional Posicionamiento de la marca Producto Líneas de productos Proyecto Razón por qué Nota del vendedor Alcance Línea de venta Nota del área de Servicio al Cliente  Estado Mercado objetivo Público Objetivo Tiempo en el mercado Your next actions should flow efficiently: confirm the Brief
                to a Sale Order, then create the Invoice and collect the Payment. mail.activity.brief mail.activity.sale mail.activity.sale.brief mail.followers.brief mail.message.brief sales.brief sales.brief.product 