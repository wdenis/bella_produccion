# -*- coding: utf-8 -*-
{
    'name': "email_task",

    'summary': """
        Correction of tasks emails
        """,
    'description': """
        Correction of tasks emails
    """,

    'author': "Rafael E. Requena G.",
    'website': "https://bella.markdebrand.com",
    'category': 'Project',
    'version': '0.1',
    'depends': ['base','project'],
    'data': [
    
    ],    
    'demo': [
    
    ],
}