# -*- coding: utf-8 -*-
from odoo import fields, models


class ProjectProject(models.Model):
    _inherit = "project.project"
    x_project_type  = fields.Selection([('internal','Internal'),('external','External')],'Project Type')
