# -*- coding: utf-8 -*-
{
    'name': "filter_multi_company",

    'summary': """
        Filter by company in session""",

    'description': """
        Filter by company in session
    """,

    'author': "Rafael E. Requena G.",
    'website': "https://bella.markdebrand.com",
    'category': 'Sales',
    'version': '0.1',
    'depends': ['base','product'],
    'data': [
        'security/contact_security.xml',
        'security/product_security.xml',
        'security/invoicing_security.xml',
    ],    
    'demo': [
    
    ],
}